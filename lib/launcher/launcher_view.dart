import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:anud4x/preferences.dart';
import 'package:anud4x/login/login_page.dart';
import 'package:anud4x/home/home_page.dart';

class LauncherPage extends StatefulWidget {
  @override
  _LauncherPageState createState() => new _LauncherPageState();
}

class _LauncherPageState extends State<LauncherPage> {
  Position _currentPosition;
  SharedPref sharedPref = SharedPref();
  String memberId;
  bool isLogged = false;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
    startLaunching();
  }

  _getCurrentLocation() {
    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best,
            forceAndroidLocationManager: true)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
    }).catchError((e) {
      print(e);
    });
  }

  startLaunching() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  loadSharedPrefs() async {
    try {
      memberId = await sharedPref.getPref("member_id");

      if (memberId != null) {
        isLogged = true;
        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
              builder: (context) => new HomePage(),
            ),
            (Route<dynamic> route) => false);
      } else {
        isLogged = false;
      }
    } catch (e) {
      isLogged = false;
      print("member_id error${e.toString()}");
    }
  }

  void navigationPage() async {
    try {
      memberId = await sharedPref.getPref("member_id");

      if (memberId != null) {
        isLogged = true;
        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
              builder: (context) => new HomePage(),
            ),
            (Route<dynamic> route) => false);
      } else {
        isLogged = false;
        Navigator.of(context)
            .pushReplacement(new MaterialPageRoute(builder: (_) {
          return new LoginPage();
        }));
      }
    } catch (e) {
      isLogged = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Image.asset(
          "assets/images/logonew.jpeg",
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
