class PaymentModel {
  String _name, _date, _hour;
  double _amount;
  int _paymentType;

  PaymentModel(
      this._name, this._date, this._hour, this._amount, this._paymentType);

  String get name => _name;
  String get date => _date;
  String get hour => _hour;
  double get amount => _amount;
  int get type => _paymentType;
}
