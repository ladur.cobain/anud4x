import 'package:anud4x/demo/payment_model.dart';
import 'package:anud4x/demo/user_model.dart';

List<PaymentModel> getPaymentsCard() {
  List<PaymentModel> paymentCards = [
    PaymentModel("Florenti Restaurant", "07-23", "20.04", 251.00, -1),
    PaymentModel("Transfer To Anna", "07-23", "14.01", 64.00, -1),
    PaymentModel("Loan To Sanchez", "07-23", "10.04", 1151.00, -1),
    PaymentModel("Train ticket to Turkey", "07-23", "09.04", 37.00, -1),
  ];

  return paymentCards;
}

List<UserModel> getUsersCard() {
  List<UserModel> userCards = [
    UserModel("Anna", "assets/images/users/anna.jpeg"),
    UserModel("Gillian", "assets/images/users/gillian.jpeg"),
    UserModel("Judith", "assets/images/users/judith.jpeg"),
  ];

  return userCards;
}
