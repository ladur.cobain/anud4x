import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:anud4x/preferences.dart';
import 'package:anud4x/services.dart';

import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:anud4x/home/home_page.dart';
import 'package:anud4x/onboarding/tab_page.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  SharedPref sharedPref = SharedPref();
  String url = ApiService.setLogin;
  String memberId;
  String message;
  bool isLogged = false;
  bool loader = false;
  bool isProcess = false;

  final primaryColor = Colors.red[600];
  final backgroundColor = Colors.white;

  bool _obscurePassword = true;
  String username, password;
  final _username = TextEditingController();
  final _password = TextEditingController();

  void _setLogin(username, password) async {
    if ((username != "") && (password != "")) {
      setState(() {
        loader = true;
        isProcess = true;
      });

      _setData(username, password);
    } else {
      message = "Please enter some text";
      Toast.show(message, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
  }

  void _setData(username, password) async {
    try {
      var response = await http.post(Uri.parse(url),
          headers: {'Accept': 'application/json'},
          body: {"username": username, "password": password});
      var content = json.decode(response.body);
      message = content['message'];

      if (content['status'] == 1) {
        isLogged = true;
        sharedPref.setPref(
            "member_id", content['result']['member_id'].toString());
        sharedPref.setPref("member_name", content['result']['member_name']);

        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
              builder: (context) => new HomePage(),
            ),
            (Route<dynamic> route) => false);
      } else {
        Toast.show(message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      }
    } catch (e) {
      print(e);
    }

    setState(() {
      loader = false;
      isProcess = false;
    });
  }

  loadSharedPrefs() async {
    try {
      memberId = await sharedPref.getPref("member_id");

      if (memberId != null) {
        isLogged = true;
        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
              builder: (context) => new HomePage(),
            ),
            (Route<dynamic> route) => false);
      } else {
        isLogged = false;
      }
    } catch (e) {
      isLogged = false;
      print("member_id error${e.toString()}");
    }
  }

  @override
  void initState() {
    loadSharedPrefs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final txtUsername = TextFormField(
      controller: _username,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Enter your email',
        hintStyle: TextStyle(color: Colors.grey),
      ),
    );

    final txtPassword = TextFormField(
      controller: _password,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Enter your password',
        hintStyle: TextStyle(color: Colors.grey),
        suffixIcon: GestureDetector(
          onTap: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
          child:
              Icon(_obscurePassword ? Icons.visibility : Icons.visibility_off),
        ),
      ),
    );

    final btnLogin = FlatButton(
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0)),
      splashColor: this.primaryColor,
      color: this.primaryColor,
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text(
              "LOGIN",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ),
          new Expanded(
            child: Container(),
          ),
          new Transform.translate(
            offset: Offset(15.0, 0.0),
            child: new Container(
              padding: const EdgeInsets.all(5.0),
              child: FlatButton(
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(28.0)),
                splashColor: Colors.white,
                color: Colors.white,
                child: Icon(
                  Icons.login,
                  color: this.primaryColor,
                ),
                onPressed: () {
                  _setLogin(_username.text, _password.text);
                },
              ),
            ),
          )
        ],
      ),
      onPressed: () {
        _setLogin(_username.text, _password.text);
      },
    );

    final _media = MediaQuery.of(context).size;
    final _clipperHeight = _media.longestSide <= 775 ? 275.0 : 375.0;
    final _sizeHeight = (_media.height - _clipperHeight) - 300.0;

    return Scaffold(
        body: Center(
            child: !isLogged
                ? ModalProgressHUD(
                    inAsyncCall: loader,
                    child: !isProcess
                        ? SingleChildScrollView(
                            child: Stack(
                              children: <Widget>[
                                new Container(
                                  height: _media.height,
                                  decoration: BoxDecoration(
                                    color: this.backgroundColor,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new ClipPath(
                                        clipper: MyClipper(),
                                        child: Container(
                                          height: _clipperHeight,
                                          decoration: BoxDecoration(
                                            image: new DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/logonew.jpeg"),
                                              fit: BoxFit.fitWidth,
                                            ),
                                          ),
                                          alignment: Alignment.center,
                                        ),
                                      ),
                                      SizedBox(height: _sizeHeight),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey.withOpacity(0.5),
                                            width: 1.0,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 10.0, horizontal: 20.0),
                                        child: Row(
                                          children: <Widget>[
                                            new Padding(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 10.0,
                                                  horizontal: 15.0),
                                              child: Icon(
                                                Icons.person_outline,
                                                color: Colors.grey,
                                              ),
                                            ),
                                            Container(
                                              height: 30.0,
                                              width: 1.0,
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              margin: const EdgeInsets.only(
                                                  left: 00.0, right: 10.0),
                                            ),
                                            new Expanded(
                                              child: txtUsername,
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey.withOpacity(0.5),
                                            width: 1.0,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 10.0, horizontal: 20.0),
                                        child: Row(
                                          children: <Widget>[
                                            new Padding(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 10.0,
                                                  horizontal: 15.0),
                                              child: Icon(
                                                Icons.lock_open,
                                                color: Colors.grey,
                                              ),
                                            ),
                                            Container(
                                              height: 30.0,
                                              width: 1.0,
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              margin: const EdgeInsets.only(
                                                  left: 00.0, right: 10.0),
                                            ),
                                            new Expanded(
                                              child: txtPassword,
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin:
                                            const EdgeInsets.only(top: 20.0),
                                        padding: const EdgeInsets.only(
                                            left: 20.0, right: 20.0),
                                        child: new Row(
                                          children: <Widget>[
                                            new Expanded(
                                              child: btnLogin,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.only(
                                            top: 5.0, bottom: 2.0),
                                        padding: const EdgeInsets.only(
                                            left: 20.0, right: 20.0),
                                        child: new Row(
                                          children: <Widget>[
                                            new Expanded(
                                              child: FlatButton(
                                                shape:
                                                    new RoundedRectangleBorder(
                                                  borderRadius:
                                                      new BorderRadius.circular(
                                                          30.0),
                                                ),
                                                color: Colors.transparent,
                                                child: Container(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 20.0),
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                    "LAPORAN",
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.red[600]),
                                                  ),
                                                ),
                                                onPressed: () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          TabPage(),
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                  )
                : Container()));
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.85);
    p.arcToPoint(
      Offset(0.0, size.height * 0.85),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
