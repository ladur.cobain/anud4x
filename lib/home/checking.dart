import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:anud4x/services.dart';
import 'package:anud4x/preferences.dart';
import 'package:toast/toast.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:anud4x/home/home_page.dart';

class CheckingPage extends StatefulWidget {
  final String lat, lng;

  CheckingPage({
    @required this.lat,
    @required this.lng,
  });

  @override
  _CheckingPageState createState() => new _CheckingPageState();
}

class _CheckingPageState extends State<CheckingPage> {
  String latitude, longitude;
  SharedPref sharedPref = SharedPref();
  static String memberId;

  String url = ApiService.checkCheckpoint;
  List listData;
  String message;
  bool loader = false;
  bool isProcess = false;

  _fetchDataPref() async {
    memberId = await sharedPref.getPref("member_id");
  }

  _getData(String member, String lats, String lngs) async {
    setState(() {
      loader = true;
      isProcess = true;
    });

    try {
      var response = await http.post(Uri.parse(url),
          body: {"member": member, "lat": lats, "lng": lngs});
      var content = json.decode(response.body);
      message = content['message'];

      setState(() {
        loader = false;
        isProcess = false;
      });

      if (content['status'] == 1) {
        setState(() {
          listData = content['result']['data'];
        });
      } else {
        Toast.show(message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      }
    } catch (e) {
      setState(() {
        loader = false;
        isProcess = false;
      });

      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchDataPref();

    latitude = widget.lat;
    longitude = widget.lng;
    _getData(memberId, latitude, longitude);
  }

  Widget listingAvailable(_media) {
    return Container(
      margin: EdgeInsets.all(10.0),
      height: _media.height - 100,
      child: ListView(
        padding: EdgeInsets.zero,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Container(
            color: Colors.grey.shade50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (OverscrollIndicatorNotification overscroll) {
                    overscroll.disallowGlow();
                    return false;
                  },
                  child: ListView.separated(
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    padding: EdgeInsets.zero,
                    itemCount: listData == null ? 0 : listData.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: ListTile(
                          onTap: () {
                            listData[index]['disabled'] == 0
                                ? _openPopup(
                                    context,
                                    listData[index]['location_id'],
                                    listData[index]['location_name'])
                                : _onBasicAlertPressed(context);
                          },
                          dense: true,
                          trailing: Text(
                            listData[index]['location_radius'],
                            style: TextStyle(
                                color: listData[index]['disabled'] == 1
                                    ? Colors.grey
                                    : Colors.black,
                                fontWeight: FontWeight.w700,
                                fontSize: 16.0),
                          ),
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                listData[index]['location_code'],
                                style: TextStyle(
                                    color: listData[index]['disabled'] == 1
                                        ? Colors.grey
                                        : Colors.black,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 18.0),
                              ),
                            ],
                          ),
                          subtitle: Padding(
                            padding: const EdgeInsets.only(top: 3.0),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      listData[index]['location_name'],
                                      style: TextStyle(
                                        color: listData[index]['disabled'] == 1
                                            ? Colors.grey
                                            : Colors.black,
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget listAvailableUI(_media) {
    return Container(
      color: Colors.grey.shade50,
      width: _media.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          listData != null
              ? listingAvailable(_media)
              : Container(
                  height: _media.height - 100,
                  padding: EdgeInsets.only(top: 15.0),
                  child: Center(
                    child: Text("No Data Found"),
                  ),
                ),
        ],
      ),
    );
  }

  _onBasicAlertPressed(context) {
    Alert(
      context: context,
      title: "INFORMATION",
      desc: "Cannot process data",
      buttons: [
        DialogButton(
          child: Text(
            "CLOSE",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          color: Colors.blue,
          onPressed: () => Navigator.pop(context),
        )
      ],
    ).show();
  }

  _openPopup(context, id, name) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "PROCESS DATA",
      desc: name,
      buttons: [
        DialogButton(
          child: Text(
            "NO",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          color: Colors.red,
          onPressed: () => Navigator.pop(context),
        ),
        DialogButton(
          child: Text(
            "YES",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          color: Colors.green,
          onPressed: () {
            _setCheckpoint(id);
          },
        )
      ],
    ).show();
  }

  void _setCheckpoint(locationId) async {
    try {
      var response = await http.get(Uri.parse(
          ApiService.setCheckpoint + '?m=' + memberId + '&l=' + locationId));
      var content = json.decode(response.body);
      message = content['message'];

      if (content['status'] == 1) {
        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
              builder: (context) => new HomePage(),
            ),
            (Route<dynamic> route) => false);
      } else {
        Toast.show(message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      }
    } catch (e) {
      print(e);
    }

    setState(() {
      loader = false;
      isProcess = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('Checkpoint'),
        backgroundColor: Colors.black87,
      ),
      body: ModalProgressHUD(
        inAsyncCall: loader,
        child: !isProcess
            ? listAvailableUI(_media)
            : Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
      ),
    );
  }
}
