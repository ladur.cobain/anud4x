import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:anud4x/preferences.dart';
import 'package:anud4x/services.dart';
import 'package:toast/toast.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:anud4x/launcher/launcher_view.dart';
import 'package:anud4x/webview.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:anud4x/onboarding/tab_page.dart';
import 'package:anud4x/home/qr_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Position _currentPosition;
  static double _currentLatitude, _currentLongitude;
  static String finalDate, finalTime;
  SharedPref sharedPref = SharedPref();
  static String memberId, memberName, memberComunity;

  String url = ApiService.getDetail;
  List listData;
  List locations;
  String message;
  bool loader = false;
  bool isProcess = false;

  _fetchDataPref() async {
    memberId = await sharedPref.getPref("member_id");
    memberName = await sharedPref.getPref("member_name");

    _getData(memberId);
  }

  _getData(String member) async {
    setState(() {
      loader = true;
      isProcess = true;
    });

    try {
      var response = await http.post(Uri.parse(url), body: {"member": member});
      var content = json.decode(response.body);
      message = content['message'];

      setState(() {
        loader = false;
        isProcess = false;
      });

      if (content['status'] == 1) {
        setState(() {
          listData = content['result']['data'];
          if (listData.length == 0) {
            listData = null;
          }

          memberName = content['result']['member']['member_name'];
          memberComunity = content['result']['member']['member_comunity'];

          locations = content['result']['locations'];
          if (locations.length == 0) {
            locations = null;
          }
        });
      } else {
        Toast.show(message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      }
    } catch (e) {
      setState(() {
        loader = false;
        isProcess = false;
      });

      print(e);
    }
  }

  _getCurrentLocation() {
    //var date = new DateTime.now().toString();
    //var dateParse = DateTime.parse(date);
    //var formattedDate = "${dateParse.day}-${dateParse.month}-${dateParse.year}";
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd').format(now);
    String formattedTime = DateFormat('hh:mm:ss').format(now);

    Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.best,
            forceAndroidLocationManager: true)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        _currentLatitude = _currentPosition.latitude;
        _currentLongitude = _currentPosition.longitude;

        finalDate = formattedDate.toString();
        finalTime = formattedTime.toString();
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  void initState() {
    super.initState();
    this._fetchDataPref();
    _getCurrentLocation();
  }

  Widget profileMemberUI(_media) {
    return Container(
      color: Colors.grey.shade50,
      height: _media.longestSide <= 775 ? 275.0 : 375.0,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Stack(
                  children: <Widget>[
                    Material(
                      elevation: 4,
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/images/logonew.jpeg"),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ),
                    Opacity(
                      opacity: 0.3,
                      child: Container(
                        color: Colors.black12,
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(),
              )
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: EdgeInsets.only(
                left: 10,
                right: 10,
              ),
              padding: EdgeInsets.only(top: 5.0, bottom: 10.0),
              height: 100.0,
              width: _media.width,
              child: NotificationListener<OverscrollIndicatorNotification>(
                onNotification: (overscroll) {
                  overscroll.disallowGlow();
                },
                child: Material(
                  elevation: 1,
                  shadowColor: Colors.grey.shade300,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: _media.width - 40,
                        padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              memberName != null ? memberName : "",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              memberComunity != null ? memberComunity : "",
                              style:
                                  Theme.of(context).textTheme.headline.copyWith(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                      ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: 15,
                        right: 15,
                        child: Column(children: [
                          Container(
                            height: 25,
                            width: 60,
                            color: Colors.red[600],
                            padding: EdgeInsets.all(5),
                            child: GestureDetector(
                              onTap: () => {showAlertDialog(context)},
                              child: Text(
                                "Logout",
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12),
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(bottom: 2.0),
                            child: FlatButton(
                              color: Colors.transparent,
                              child: Container(
                                //padding: const EdgeInsets.only(left: 20.0),
                                alignment: Alignment.center,
                                child: Text(
                                  "QR Code",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red[600]),
                                ),
                              ),
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        GenerateQR(
                                      id: memberId.toString(),
                                      name: memberName,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget listLocation() {
    return Container(
      margin: EdgeInsets.only(left: 25),
      height: 60,
      child: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
        },
        child: ListView.builder(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: locations == null ? 0 : locations.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                _getCurrentLocation();

                if (_currentPosition != null) {
                  String c = _currentLatitude.toString() +
                      "," +
                      _currentLongitude.toString();
                  String l = locations[index]['location_latlon'].toString();
                  String d = locations[index]['location_radius'].toString();
                  String n = locations[index]['location_name'].toString();
                  String m = memberName;

                  String selectedUri =
                      "https://lacakmt.ct-tracking.com/api-anud4x/map/check?d=" +
                          d +
                          "&c=" +
                          c +
                          "&l=" +
                          l +
                          "&m=" +
                          m +
                          "&n=" +
                          n;

                  var encoded = Uri.encodeFull(selectedUri);

                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => MyWebView(
                        title: n.toString(),
                        selectedUrl: encoded,
                      ),
                    ),
                  );
                } else {
                  Toast.show("GPS Location Error, harap coba lagi.", context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                }
              },
              child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: Container(
                  alignment: Alignment.center,
                  width: 100.0,
                  padding: EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.lightBlue.shade50,
                        blurRadius: 8.0,
                        spreadRadius: 4,
                      ),
                      BoxShadow(
                        color: Colors.white,
                        blurRadius: 10.0,
                      ),
                    ],
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        locations[index]['location_code'],
                        style: TextStyle(
                            inherit: true,
                            fontWeight: FontWeight.w500,
                            fontSize: 16.0,
                            color: Colors.black),
                        overflow: TextOverflow.fade,
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: Text(
                          locations[index]['location_slug'],
                          style: TextStyle(
                              inherit: true,
                              fontWeight: FontWeight.w500,
                              fontSize: 14.0,
                              color: Colors.grey),
                          overflow: TextOverflow.fade,
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget listLocationUI(_media) {
    return Container(
      color: Colors.grey.shade50,
      width: _media.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(left: 10.0, right: 10, bottom: 5, top: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "CP Selengkapnya ...",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                    size: 14,
                  ),
                  tooltip: "CP Selengkapnya ...",
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => MyWebView(
                          title: "All Locations",
                          selectedUrl:
                              "https://lacakmt.ct-tracking.com/api-anud4x/map/multiple",
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          locations != null
              ? listLocation()
              : Container(
                  padding: EdgeInsets.only(top: 5.0, bottom: 15.0),
                  child: Center(
                    child: Text("No Data Found"),
                  ),
                ),
        ],
      ),
    );
  }

  Widget listCheckpoint() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (overscroll) {
            overscroll.disallowGlow();
          },
          child: ListView.separated(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            separatorBuilder: (context, index) {
              return Divider();
            },
            padding: EdgeInsets.zero,
            itemCount: listData == null ? 0 : listData.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: ListTile(
                  dense: true,
                  trailing: Text(
                    listData[index]['checkpoint_distance'],
                    style: TextStyle(
                        inherit: true,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0),
                  ),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        listData[index]['checkpoint_name'],
                        style: TextStyle(
                            inherit: true,
                            fontWeight: FontWeight.w700,
                            fontSize: 16.0),
                      ),
                    ],
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(listData[index]['checkpoint_date'],
                            style: TextStyle(
                                inherit: true,
                                fontSize: 12.0,
                                color: Colors.black45)),
                        SizedBox(
                          width: 5,
                        ),
                        Text(listData[index]['checkpoint_time'],
                            style: TextStyle(
                                inherit: true,
                                fontSize: 12.0,
                                color: Colors.black45)),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget listCheckpointUI(_media) {
    return Container(
      color: Colors.grey.shade50,
      width: _media.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                left: 10.0, right: 10, bottom: 5, top: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Laporan ...",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.grey,
                    size: 14,
                  ),
                  tooltip: "Laporan ...",
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TabPage(),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          listData != null
              ? listCheckpoint()
              : Container(
                  padding: EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text("No Data Found"),
                  ),
                ),
        ],
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(false);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        sharedPref.dropPref("member_id");
        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
              builder: (context) => new LauncherPage(),
            ),
            (Route<dynamic> route) => false);
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("are you sure want to logout ?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _setLocation(latitude, longitude, d, t) async {
    try {
      var response = await http.get(Uri.parse(ApiService.setLocation +
          '?m=' +
          memberId +
          '&lat=' +
          latitude +
          '&lng=' +
          longitude +
          '&d=' +
          d +
          '&t=' +
          t));

      var content = json.decode(response.body);
      message = content['message'];
      _onBasicAlertPressed(context, message, content['status'].toString());
    } catch (e) {
      print(e);
    }

    setState(() {
      loader = false;
      isProcess = false;
    });
  }

  _onBasicAlertPressed(context, msg, status) {
    Alert(
      context: context,
      title: "INFORMATION",
      desc: msg,
      buttons: [
        DialogButton(
          child: Text(
            "CLOSE",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          color: status == "1" ? Colors.green : Colors.red,
          onPressed: () => status == "1"
              ? Navigator.of(context).pushAndRemoveUntil(
                  new MaterialPageRoute(
                    builder: (context) => new HomePage(),
                  ),
                  (Route<dynamic> route) => false)
              : Navigator.of(context, rootNavigator: true).pop(false),
        )
      ],
    ).show();
  }

  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: ModalProgressHUD(
        inAsyncCall: loader,
        child: !isProcess
            ? ListView(
                padding: EdgeInsets.zero,
                physics: BouncingScrollPhysics(),
                children: <Widget>[
                    profileMemberUI(_media),
                    listLocationUI(_media),
                    listCheckpointUI(_media)
                  ])
            : Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _getCurrentLocation();
          if (_currentPosition != null) {
            String lat = _currentLatitude.toString();
            String lng = _currentLongitude.toString();
            //print(finalDate + ' ' + finalTime);
            _setLocation(lat, lng, finalDate, finalTime);
          } else {
            Toast.show("GPS Location Error, harap coba lagi.", context,
                duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          }
        },
        label: const Text('CHECK IN'),
        icon: const Icon(Icons.location_pin),
        backgroundColor: Colors.red[600],
      ),
    );
  }
}
