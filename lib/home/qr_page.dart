import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class GenerateQR extends StatelessWidget {
  final String id;
  final String name;

  GenerateQR({
    @required this.id,
    @required this.name,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(name),
        backgroundColor: Colors.black87,
      ),
      body: Center(
        child: QrImage(data: id),
      ),
    );
  }
}
