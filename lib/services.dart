class ApiService {
  static String baseUrl = "https://lacakmt.ct-tracking.com/api-anud4x/";
  static String setLogin = baseUrl + "member/set-login";
  static String getDetail = baseUrl + "member/get-detail";
  static String setCheckpoint = baseUrl + "transaction/set-checkpoint";
  static String getOnboarding = baseUrl + "transaction/get-onboarding";
  static String checkCheckpoint = baseUrl + "transaction/check-checkpoint";
  static String setLocation = baseUrl + "transaction/set-location";
}
