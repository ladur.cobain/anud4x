import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:anud4x/services.dart';
import 'package:toast/toast.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class OnboardingPage extends StatefulWidget {
  @override
  _OnboardingPageState createState() => new _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  String url = ApiService.getOnboarding;
  List listData;
  String message;
  bool loader = false;
  bool isProcess = false;

  _getData() async {
    setState(() {
      loader = true;
      isProcess = true;
    });

    try {
      var response = await http.get(Uri.parse(url));
      var content = json.decode(response.body);
      message = content['message'];

      setState(() {
        loader = false;
        isProcess = false;
      });

      if (content['status'] == 1) {
        setState(() {
          listData = content['result']['data'];
          if (listData.length == 0) {
            listData = null;
          }
        });
      } else {
        Toast.show(message, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      }
    } catch (e) {
      setState(() {
        loader = false;
        isProcess = false;
      });

      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    this._getData();
  }

  Widget listingOnboarding(_media) {
    return Container(
      margin: EdgeInsets.all(10.0),
      height: _media.height - 325,
      child: ListView(
        padding: EdgeInsets.zero,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Container(
            color: Colors.grey.shade50,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (OverscrollIndicatorNotification overscroll) {
                    overscroll.disallowGlow();
                    return false;
                  },
                  child: ListView.separated(
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                    padding: EdgeInsets.zero,
                    itemCount: listData == null ? 0 : listData.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: ListTile(
                          dense: true,
                          trailing: Text(
                            listData[index]['diff'],
                            style: TextStyle(
                                inherit: true,
                                fontWeight: FontWeight.w700,
                                fontSize: 16.0),
                          ),
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                listData[index]['member'],
                                style: TextStyle(
                                    inherit: true,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 18.0),
                              ),
                            ],
                          ),
                          subtitle: Padding(
                            padding: const EdgeInsets.only(top: 3.0),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      listData[index]['location'],
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.black54),
                                    ),
                                    Text(
                                      listData[index]['date'] +
                                          ' ' +
                                          listData[index]['time'],
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          color: Colors.black45),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget listOnboardingUI(_media) {
    return Container(
      color: Colors.grey.shade50,
      width: _media.width,
      height: _media.height - 165,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          listData != null
              ? listingOnboarding(_media)
              : Container(
                  padding: EdgeInsets.only(top: 25.0),
                  child: Center(
                    child: Text("No Data Found"),
                  ),
                ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context).size;

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
                expandedHeight: 225.0,
                floating: false,
                pinned: true,
                backgroundColor: Colors.black87,
                flexibleSpace: FlexibleSpaceBar(
                  background: Stack(
                    children: <Widget>[
                      Positioned.fill(
                          child: Image.asset(
                        "assets/images/logonew.jpeg",
                        fit: BoxFit.cover,
                      )),
                    ],
                  ),
                )),
          ];
        },
        body: ModalProgressHUD(
          inAsyncCall: loader,
          child: !isProcess
              ? Center(
                  child: listOnboardingUI(_media),
                )
              : Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
        ),
      ),
    );
  }
}
